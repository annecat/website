build:
	echo "building"
	mkdir dst
	cp -r tara dst
	cp -r rachael dst
	cp -r www dst
test:
	echo "nothing to test yet"
srht: build
	# rsync -rv --exclude Makefile --exclude README * zie@zie.one:/var/www/htdocs/anne.cat/
	echo "building"
	cp -r  dst ../websites/anne.cat/
	cd ../websites; git add anne.cat/*; git commit -a -m "built anne.cat websiter"; git push
	#ssh zie@zie.one cd /var/www/htdocs/anne.cat; git pull
clean:
	rm -rf dst/*
gitlab: build
	echo "publishing via gitlab CI/CD"
	mkdir -p ~/.ssh
	echo "$(GIT_SR_HT_HOST_KEY)" > ~/.ssh/known_hosts
	git config --global user.email "annecat@gitlab.com"
	git config --global user.name "anne.cat gitlab builder"
	git clone git@git.sr.ht:~zie/websites
	cp -r  dst/* websites/anne.cat/
	cd websites; git add anne.cat/*; git commit -a -m "built anne.cat website via gitlab build: $(BUILD_NUMBER)"; git push
